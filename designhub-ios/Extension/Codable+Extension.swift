//
//  Codable+Extension.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation

extension Encodable {

    var asDictionary : [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        guard let json = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] else { return nil }
        return json
    }
}
