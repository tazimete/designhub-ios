//
//  UIImageView+Extension.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import UIKit

extension UIImageView {
    func loadImage(from url: String, placeholderImage: UIImage? = UIImage(named: "img_avatar"), completionHandler: @escaping (DownloadCompletionHandler<UIImage>)){
        
//        self.image = placeholderImage
        ImageDownloader.shared.download(with: url, completionHandler: completionHandler, placeholder: placeholderImage)
    }
}

