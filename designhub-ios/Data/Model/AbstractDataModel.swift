//
//  AbstractDataModel.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation


/*
 Base class for our server response
 */

public protocol AbstractDataModel: AnyObject {
    var id: Int? {get set}
    
    //dictionary representation of this model 
    var asDictionary : [String: Any]? {get}
}

