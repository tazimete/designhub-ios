//
//  MovieRequestParameter.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import Foundation


struct GenreParams: Parameterizable{
    let apiKey: String = AppConfig.shared.getServerConfig().setAuthCredential().apiKey ?? ""
    let language: String

    public init(language: String) {
        self.language = language
    }

    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case language = "language"
    }

    public var asRequestParam: [String: Any] {
        let param: [String: Any] = [CodingKeys.apiKey.rawValue: apiKey, CodingKeys.language.rawValue: language]
        return param.compactMapValues { $0 }
    }
}
