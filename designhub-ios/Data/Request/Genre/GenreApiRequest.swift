//
//  MoviewRequestApi.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation

enum GenreApiRequest {
    case getGenreList(params: Parameterizable)
}

extension GenreApiRequest: APIRequest {
    public var baseURL: URL {
        let url =  "\(AppConfig.shared.getServerConfig().getBaseUrl())/\(AppConfig.shared.getServerConfig().getApiVersion())/"
        return URL(string: url)!
    }
    
    public typealias ItemType = Genre
    public typealias ResponseType = GenreResponse<ItemType>
    
    public var method: RequestType {
        switch self {
            case .getGenreList: return .GET
        }
    }
    
    public var path: String {
        switch self {
            case .getGenreList: return "/genre/movie/list"
        }
    }
    
    public var parameters: [String: Any]{
        var parameter: [String: Any] = [:]
        
        switch self {
            case .getGenreList (let params):
                parameter = params.asRequestParam
        }
        
        return parameter
    }
    
    public var headers: [String: String] {
        return [String: String]()
    }
}


