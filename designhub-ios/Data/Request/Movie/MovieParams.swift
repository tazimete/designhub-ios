//
//  GithubUserParams.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation


struct MovieParams: Parameterizable{
    let apiKey: String = AppConfig.shared.getServerConfig().setAuthCredential().apiKey ?? ""
    let page: Int

    public init(page: Int) {
        self.page = page
    }

    private enum CodingKeys: String, CodingKey {
        case apiKey = "api_key"
        case page = "page"
    }

    public var asRequestParam: [String: Any] {
        let param: [String: Any] = [CodingKeys.apiKey.rawValue: apiKey, CodingKeys.page.rawValue: page]
        return param.compactMapValues { $0 }
    }
}
