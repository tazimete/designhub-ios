//
//  Network.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation

enum MovieApiRequest {
    case fetchUpcomingMovie(params: Parameterizable)
    case fetchPopularMovie(params: Parameterizable)
}

extension MovieApiRequest: APIRequest {
    public var baseURL: URL {
        let url =  "\(AppConfig.shared.getServerConfig().getBaseUrl())/\(AppConfig.shared.getServerConfig().getApiVersion())"
        return URL(string: url)!
    }
    
    public typealias ItemType = Movie
    public typealias ResponseType = MovieResponse<ItemType>
    
    public var method: RequestType {
        switch self {
            case .fetchUpcomingMovie: return .GET
            case .fetchPopularMovie: return .GET
        }
    }
    
    public var path: String {
        switch self {
            case .fetchUpcomingMovie: return "/movie/upcoming"
            case .fetchPopularMovie: return "/movie/popular"
        }
    }
    
    public var parameters: [String: Any]{
        var parameter: [String: Any] = [:]
        
        switch self {
            case .fetchUpcomingMovie (let params):
                parameter = params.asRequestParam
            case .fetchPopularMovie (let params):
                parameter = params.asRequestParam
        }
        
        return parameter
    }
    
    public var headers: [String: String] {
        return [String: String]()
    }
}


