//
//  MovieUsecase.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

/* This is Genre usecase class implementation from AbstractGenreUsecase. Which will be used to get genre related data from genre repository*/
class GenreUsecase: AbstractGenreUsecase {
    
    var repository: AbstractRepository
    
    public init(repository: AbstractGenreRepository) {
        self.repository = repository
    }
    
    func fetchGenres(language: String) -> Observable<GenreApiRequest.ResponseType> {
        return (repository as! AbstractGenreRepository).get(language: language)
    }
}
