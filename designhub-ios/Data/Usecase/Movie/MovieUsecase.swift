//
//  SearchUsecase.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

/* This is Movie usecase class implentation from AbstractMovieUsecase. Which will be used to get movie related data from movie repository*/
class MovieUsecase: AbstractMovieUsecase {
    
    var repository: AbstractRepository
    
    public init(repository: AbstractMovieRepository) {
        self.repository = repository
    }
    
    func fetchUpcommingMovies(page: Int) -> Observable<MovieApiRequest.ResponseType> {
        return (repository as! AbstractMovieRepository).getUpcomming(page: page)
    }
    
    func fetchPopularMovies(page: Int) -> Observable<MovieApiRequest.ResponseType> {
        return (repository as! AbstractMovieRepository).getPopular(page: page)
    }
}
