//
//  MovieRepository.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

/* This is Movie repository class implementation from AbstractMovieRepository. Which will be used to get movie related from api client/server response*/
class MovieRepository: AbstractMovieRepository {
    
    var apiClient: AbstractApiClient
    
    init(apiClient: AbstractApiClient = APIClient.shared) {
        self.apiClient = apiClient
    }
    
    func getUpcomming(page: Int) -> Observable<MovieApiRequest.ResponseType> {
        return apiClient.send(apiRequest: MovieApiRequest.fetchUpcomingMovie(params: MovieParams(page: page)), type: MovieApiRequest.ResponseType.self)
    }
    
    func getPopular(page: Int) -> Observable<MovieApiRequest.ResponseType> {
        return apiClient.send(apiRequest: MovieApiRequest.fetchPopularMovie(params: MovieParams(page: page)), type: MovieApiRequest.ResponseType.self)
    }
}
