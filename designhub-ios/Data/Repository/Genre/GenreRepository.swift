//
//  MovieRepository.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

/* This is Genre repository class implementation from AbstractGenreRepository. Which will be used to get genre related from api client/server response*/
class GenreRepository: AbstractGenreRepository {
    
    var apiClient: AbstractApiClient
    
    init(apiClient: AbstractApiClient = APIClient.shared) {
        self.apiClient = apiClient
    }
    
    func get(language: String) -> Observable<GenreApiRequest.ResponseType> {
        return apiClient.send(apiRequest: GenreApiRequest.getGenreList(params: GenreParams(language: language)), type: GenreApiRequest.ResponseType.self)
    }
}
