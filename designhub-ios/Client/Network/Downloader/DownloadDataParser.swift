//
//  DownloadDataParser.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import UIKit


public class DownloadDataParser<T> {
    public static func getObjectAsType(data: Data) -> T? {
        var output: T?
        
        if T.self is UIImage.Type  {
            output = UIImage(data: data)?.decodedImage() as? T
        }

        return output
    }
}
