//
//  Parameterizable.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation


public protocol Parameterizable {
    var asRequestParam: [String: Any] { get }
}

