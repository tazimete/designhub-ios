//
//  AbstractApiClient.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

typealias NetworkCompletionHandler<T: Codable> = (Result<T, NetworkError>) -> Void

protocol AbstractApiClient: AnyObject {
    var session: AbstractURLSession {get set}
    var queueManager: QueueManager {get set}
    func enqueue<T: Codable>(apiRequest: APIRequest, type: T.Type, completionHandler: @escaping (NetworkCompletionHandler<T>))
    func send<T: Codable>(apiRequest: APIRequest, type: T.Type) -> Observable<T>
}
