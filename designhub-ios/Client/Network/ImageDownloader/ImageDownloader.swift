//
//  ImageDownloader.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import UIKit


public class ImageDownloader: Downloader<UIImage> {
    static let shared = ImageDownloader()

    override init() {
        super.init()
    }
}
