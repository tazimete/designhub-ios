//
//  AbstractMovieRepository.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

/* This is Movie repository abstraction extented from AbstractRepository. Which will be used to get movie related from api client/server response*/
protocol AbstractMovieRepository: AbstractRepository {
    func getUpcomming(page: Int) -> Observable<MovieApiRequest.ResponseType>
    func getPopular(page: Int) -> Observable<MovieApiRequest.ResponseType>
}
