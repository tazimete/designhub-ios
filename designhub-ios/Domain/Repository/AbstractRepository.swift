//
//  AbstractRepository.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation

/* Base repository abstraction will be used to make all other repository of this project, It will have apiClient to get data from server */
protocol AbstractRepository: AnyObject {
    var apiClient: AbstractApiClient {get}
}
