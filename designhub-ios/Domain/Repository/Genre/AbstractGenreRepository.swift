//
//  AbstractGenreRepository.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

/* This is Genre repository abstraction extented from AbstractRepository. Which will be used to get genre related from api client/server response*/
protocol AbstractGenreRepository: AbstractRepository {
    func get(language: String) -> Observable<GenreApiRequest.ResponseType>
}
