//
//  Genre.swift
//  designhub-ios
//
//  Created by AGM Tazim on 1/29/22.
//

import Foundation


/* Movie entity of search response */
struct Genre: Codable {
    public let id: Int?
    public let name: String?
    
    init(id: Int? = nil, name: String? = nil) {
        self.id = id
        self.name = name
    }
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
    
    public var asCellViewModel: AbstractCellViewModel {
        return GenreItemCellViewModel(id: id, thumbnail: name?.first?.description.capitalized, title: name)
    }
}
