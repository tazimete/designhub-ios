//
//  GenreResponse.swift
//  designhub-ios
//
//  Created by AGM Tazim on 1/29/22.
//

import Foundation


/* Wrapper response of genre api, which has array of dynamic genre */
public struct GenreResponse<T: Codable>: Codable {
    public let genres: [T]?
    
    public init(genres: [T]? = nil) {
        self.genres = genres
    }
    
    public enum CodingKeys: String, CodingKey {
        case genres = "genres"
    }
}
