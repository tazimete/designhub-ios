//
//  Entity.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation

/* Wrapper response of movie api, which has array of dynamic movie content like - movie, upcoming, genre */
public struct MovieResponse<T: Codable>: Codable {
    public let page: Int?
    public let results: [T]?
    public let totalPages: Int?
    public let totalResults: Int?
    
    public init(page: Int? = nil, results: [T]? = nil, totalPages: Int? = nil, totalResults: Int? = nil) {
        self.page = page
        self.results = results
        self.totalPages = totalPages
        self.totalResults = totalResults
    }
    
    public enum CodingKeys: String, CodingKey {
        case page = "page"
        case results = "results"
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}
