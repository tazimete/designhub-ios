//
//  AbstractSearchUsecase.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

/* This is Movie usecase abstraction extented from AbstractUsecase. Which will be used to get movie related data from movie repository*/
protocol AbstractMovieUsecase: AbstractUsecase {
    func fetchUpcommingMovies(page: Int) -> Observable<MovieApiRequest.ResponseType>
    func fetchPopularMovies(page: Int) -> Observable<MovieApiRequest.ResponseType>
}
