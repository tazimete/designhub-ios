//
//  AbstractMovieUsecase.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift

/* This is Genre usecase abstraction extented from AbstractUsecase. Which will be used to get genre related data from genre repository*/
protocol AbstractGenreUsecase: AbstractUsecase {
    func fetchGenres(language: String) -> Observable<GenreApiRequest.ResponseType>
}
