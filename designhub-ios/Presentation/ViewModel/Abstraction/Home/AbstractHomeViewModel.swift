//
//  SearchViewModel.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//


import Foundation
import RxSwift

/* This is Home viewmodel abstraction extented from AbstractViewModel. Which will be used to get movie related data by movie, genre usecase*/
protocol AbstractHomeViewModel: AbstractViewModel {
    // Extra usecase
    var genreUsecase: AbstractGenreUsecase {get}
    
    // Transform the movie input to output observable
    func getHomeOutput(input: HomeViewModel.HomeInput) -> HomeViewModel.HomeOutput
    
    // get upcoming movie list through api call
    func fetchUcomingMovies(page: Int) -> Observable<MovieApiRequest.ResponseType>
    
    // get popular movie list through api call
    func fetchPopularMovies(page: Int) -> Observable<MovieApiRequest.ResponseType>
    
    // get genre list through api call
    func fetchGenres(language: String) -> Observable<GenreApiRequest.ResponseType>
}
