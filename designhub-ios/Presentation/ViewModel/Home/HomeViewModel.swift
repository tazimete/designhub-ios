//
//  HomeViewModel.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation
import RxSwift
import RxCocoa

/* This is Home viewmodel class implementation of AbstractHomeViewModel. Which will be used to get homescreen related data by movie,genre usecase*/
class HomeViewModel: AbstractHomeViewModel {
    
    // This struct will be used get input from viewcontroller
    public struct MovieInputModel {
        let page: Int
    }
    
    // This struct will be used get event with data from viewcontroller
    public struct HomeInput {
        let fetchHomeDataTrigger: Observable<MovieInputModel>
    }
    
    // This struct will be used send event with observable data/response to viewcontroller 
    public struct HomeOutput {
        let movieData: BehaviorRelay<[CellConfigurator]>
        let errorTracker: BehaviorRelay<NetworkError?>
    }
    
    public var usecase: AbstractUsecase
    public var genreUsecase: AbstractGenreUsecase
    public let disposebag = DisposeBag()
    
    public init(usecase: AbstractUsecase, genreUsecase: AbstractGenreUsecase) {
        self.usecase = usecase
        self.genreUsecase = genreUsecase
    }
    
    public func getHomeOutput(input: HomeInput) -> HomeOutput {
        let movieListResponse = BehaviorRelay<[CellConfigurator]>(value: [])
        let errorResponse = BehaviorRelay<NetworkError?>(value: nil)
        
        input.fetchHomeDataTrigger.flatMapLatest({ [weak self] (inputModel) -> Observable<(MovieApiRequest.ResponseType, MovieApiRequest.ResponseType, GenreApiRequest.ResponseType)> in
            guard let weakSelf = self else {
                return Observable.just((MovieApiRequest.ResponseType(), MovieApiRequest.ResponseType(), GenreApiRequest.ResponseType()))
            }
            
            //show shimmer
            movieListResponse.accept(Array<CellConfigurator>(repeating: ShimmerItemCellConfig.init(item: MovieItemCellViewModel()), count: 9))
            
            //fetch movie data
            return Observable.combineLatest(weakSelf.fetchUcomingMovies(page: inputModel.page), weakSelf.fetchPopularMovies(page: inputModel.page), weakSelf.fetchGenres(language: "en")) { upcomingMovies, popularMovies, genres in
                return (upcomingMovies, popularMovies, genres)
            }.catch({ error in
                errorResponse.accept(error as? NetworkError)
              
                return  Observable.just((MovieApiRequest.ResponseType(), MovieApiRequest.ResponseType(), GenreApiRequest.ResponseType()))
             })
        }).subscribe(onNext: { upcomingMovies, popularMovies, genres in
            let upcomingCellConfigurator = UpcomingMovieSectionCellConfig.init(
                item: UpcomingMovieSectionCellViewModel(
                    data: (upcomingMovies.results ?? [Movie]())
                        .map({return $0.asCellViewModel })
                )
            )
            
            let genresCellConfigurator = GenreSectionCellConfig.init(
                item: GenreSectionCellViewModel(
                    data: (genres.genres ?? [Genre]())
                        .map({return $0.asCellViewModel })
                )
            )
            
            let popularSectionCellConfigurators = [PopularMovieSectionCellConfig.init(item: MovieItemCellViewModel())]
            let popularCellConfigurators =  (popularMovies.results ?? []).map({ return MovieItemCellConfig.init(item: $0.asCellViewModel)})
            
            movieListResponse.accept([upcomingCellConfigurator] + [genresCellConfigurator] + popularSectionCellConfigurators + popularCellConfigurators)
        }, onError: { [weak self] error in
            errorResponse.accept(error as? NetworkError)
        }, onCompleted: nil, onDisposed: nil).disposed(by: disposebag)
        
        return HomeOutput.init(movieData: movieListResponse, errorTracker: errorResponse)
    }
    
    public func fetchUcomingMovies(page: Int) -> Observable<MovieApiRequest.ResponseType> {
        return (usecase as! AbstractMovieUsecase).fetchUpcommingMovies(page: page)
    }
    
    public func fetchPopularMovies(page: Int) -> Observable<MovieApiRequest.ResponseType> {
        return (usecase as! AbstractMovieUsecase).fetchPopularMovies(page: page)
    }
    
    public func fetchGenres(language: String) -> Observable<GenreApiRequest.ResponseType> {
        return genreUsecase.fetchGenres(language: language)
    }
}
