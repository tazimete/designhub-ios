//
//  ViewController.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import UIKit
import RxSwift
import RxCocoa

class HomeViewController: BaseViewController {
    // MARK: Non UI Proeprties
    public var homeViewModel: AbstractHomeViewModel!
    private let disposeBag = DisposeBag()
    private let homeTrigger = PublishSubject<HomeViewModel.MovieInputModel>()
    private var innerListViewOffsets = [Int: CGFloat]()
    
    // MARK: UI Proeprties
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.allowsSelection = false
        tableView.separatorStyle = .none
        tableView.separatorInset = .zero
        tableView.showsHorizontalScrollIndicator = false
        tableView.showsVerticalScrollIndicator = false
        tableView.backgroundColor = .black
        
        //cell registration
        tableView.register(MovieItemCell.self, forCellReuseIdentifier: MovieItemCellConfig.reuseId)
        tableView.register(UpcomingMovieSectionCell.self, forCellReuseIdentifier: UpcomingMovieSectionCellConfig.reuseId)
        tableView.register(GenreSectionCell.self, forCellReuseIdentifier: GenreSectionCellConfig.reuseId)
        tableView.register(MovieShimmerCell.self, forCellReuseIdentifier: ShimmerItemCellConfig.reuseId)
        tableView.register(PopularMovieSectionCell.self, forCellReuseIdentifier: PopularMovieSectionCellConfig.reuseId)
        return tableView
    }()
    
    // MARK: Constructors
    init(viewModel: AbstractHomeViewModel) {
        super.init(viewModel: viewModel)
        self.viewModel = viewModel
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: Overrriden Methods
    override func initView() {
        super.initView()
    
        view.backgroundColor = .black
        
        //setup tableview
        view.addSubview(tableView)
        
        //set anchor
        tableView.anchor(top: view.topAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: view.frame.width, height: 0, enableInsets: true)
        
        //table view
        registerTableViewCellCallbackEvents()
    }
    
    //when theme change, we can also define dark mode color option in color asse
    override public func applyDarkTheme() {
        navigationController?.navigationBar.backgroundColor = .lightGray
        tableView.backgroundColor = .black
        self.navigationItem.rightBarButtonItem?.tintColor = .lightGray
        tableView.reloadData()
    }
    
    override public func applyNormalTheme() {
        navigationController?.navigationBar.backgroundColor = .midnightBlue
        tableView.backgroundColor = .black
        self.navigationItem.rightBarButtonItem?.tintColor = .white
        tableView.reloadData()
    }
    
    override func initNavigationBar() {
        self.navigationItem.title = "Mononton"
        navigationController?.navigationBar.barStyle = .black
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeContentTitle = "Mononton"
        navigationController?.navigationBar.tintColor = .white
    }
    
    override func bindViewModel() {
        homeViewModel = (viewModel as! AbstractHomeViewModel)
        let homeInput = HomeViewModel.HomeInput(fetchHomeDataTrigger: homeTrigger)
        let homeOutput = homeViewModel.getHomeOutput(input: homeInput)
        
        //populate table view
        homeOutput.movieData.observe(on: MainScheduler.instance)
            .bind(to: tableView.rx.items) { [weak self] tableView, row, model in
                guard let weakSelf = self else {
                    return UITableViewCell()
                }
                
                return weakSelf.populateTableViewCell(cellConfigurator: model, indexPath: IndexPath(row: row, section: 0), tableView: tableView)
            }.disposed(by: disposeBag)
        
        // detect error
        homeOutput.errorTracker.observe(on: MainScheduler.instance)
            .subscribe(onNext: {
                [weak self] error in
                
                guard let weakSelf = self, let error = error else {
                    return
                }
            
            print("\(String(describing: weakSelf.TAG)) -- bindViewModel() -- error  -- code = \(error.errorCode), message = \(error.errorMessage)")
        }).disposed(by: disposeBag)
        
        //fetch data
        fetchHomeData(page: 1)
    }
    
    public func fetchHomeData(page: Int) {
        homeTrigger.onNext(HomeViewModel.MovieInputModel(page: page))
    }
    
    
    //populate table view cell
    private func populateTableViewCell(cellConfigurator: CellConfigurator, indexPath: IndexPath, tableView: UITableView) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: cellConfigurator).reuseId, for: indexPath)
        cellConfigurator.configure(cell: cell)
        
        return cell
    }
    
    // MARK: Actions
    private func registerTableViewCellCallbackEvents() {
        // on tap tableview cell
        Observable
            .zip(tableView.rx.itemSelected, tableView.rx.modelSelected(CellConfigurator.self))
            .bind { [weak self] indexPath, model in
                guard let weakSelf = self else {
                    return
                }
                
                weakSelf.tableView.deselectRow(at: indexPath, animated: true)
            }
            .disposed(by: disposeBag)
        
        // willdisplay cell event
        tableView.rx.willDisplayCell.subscribe(onNext: { [weak self] data in
            guard let weakSelf = self, let cell = data.cell as? UpcomingMovieSectionCell else { return }
            cell.offset = weakSelf.innerListViewOffsets[data.indexPath.row] ?? 0
        }).disposed(by: disposeBag)
        
        //willend display cell event
        tableView.rx.didEndDisplayingCell.subscribe(onNext: { [weak self] data in
            guard let weakSelf = self, let cell = data.cell as? UpcomingMovieSectionCell else { return }
            weakSelf.innerListViewOffsets[data.indexPath.row] = cell.offset
        }).disposed(by: disposeBag)
    }
}

