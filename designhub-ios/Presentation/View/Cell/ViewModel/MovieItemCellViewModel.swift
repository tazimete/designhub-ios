//
//  SearchCellViewModel.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import Foundation


protocol AbstractCellViewModel: AnyObject {
    var id: Int? {set get}
    var thumbnail: String? {set get}
    var title: String? {set get}
    var overview: String? {set get}
}

class MovieItemCellViewModel: AbstractCellViewModel {
    var id: Int?
    var thumbnail: String?
    var title: String?
    var overview: String?
    var genres: [Genre]?
    var releaseDate: String?
    var voteAverage: Float?
    
    init(id: Int? = nil, thumbnail: String? = nil, title: String? = nil, overview: String? = nil, genres: [Genre]? = nil, releaseDate: String? = nil, voteAverage: Float? = nil) {
        self.id = id
        self.thumbnail = thumbnail
        self.title = title
        self.overview = overview
        self.genres = genres
        self.releaseDate = releaseDate
        self.voteAverage = voteAverage
    }
}


class GenreItemCellViewModel: MovieItemCellViewModel {

}
