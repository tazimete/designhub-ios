//
//  UpcomingMovieSectionCellViewModel.swift
//  designhub-ios
//
//  Created by AGM Tazim on 1/31/22.
//

import Foundation


class UpcomingMovieSectionCellViewModel: AbstractCellViewModel {
    var id: Int?
    var thumbnail: String?
    var title: String?
    var overview: String?
    var data: [AbstractCellViewModel]?
    
    init(id: Int? = nil, thumbnail: String? = nil, title: String? = nil, overview: String? = nil, data: [AbstractCellViewModel]? = nil) {
        self.id = id
        self.thumbnail = thumbnail
        self.title = title
        self.overview = overview
        self.data = data
    }
}
