//
//  PopularMovieSectionCell.swift
//  designhub-ios
//
//  Created by AGM Tazim on 2/3/22.
//

import UIKit


typealias PopularMovieSectionCellConfig = ListViewCellConfigurator<PopularMovieSectionCell, AbstractCellViewModel>

class PopularMovieSectionCell : UITableViewCell, ConfigurableCell {
    typealias DataType = AbstractCellViewModel
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.isSkeletonable = true
        view.layer.borderWidth = 0
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.cornerRadius = 0
        return view
    }()
    
    let lblTitle : UILabel = {
        let label = UILabel()
        label.text = "Continue watching"
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.isSkeletonable = true
        label.skeletonLineSpacing = 10
        label.multilineSpacing = 10
        return label
    }()
    
    let btnSeeAll : UIButton = {
        let button = UIButton()
        button.setTitleColor(.white, for: .normal)
        button.contentHorizontalAlignment = .right
        button.setTitle("See All", for: .normal)
        return button
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupSubViews() {
        contentView.addSubview(containerView)
        containerView.addSubview(lblTitle)
        containerView.addSubview(btnSeeAll)
        
        self.backgroundColor = .black
        let frameWidth = frame.width
        
        contentView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: frameWidth, height: 60, enableInsets: false)
        containerView.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: contentView.frame.width, height: contentView.frame.height, enableInsets: false)
        lblTitle.anchor(top: containerView.topAnchor, left: containerView.leftAnchor, bottom: containerView.bottomAnchor, right: btnSeeAll.leftAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, width: 0, height: 0, enableInsets: false)
        btnSeeAll.anchor(top: lblTitle.topAnchor, left: lblTitle.rightAnchor, bottom: lblTitle.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 5, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
    }

    public func configure(data: DataType) {
        //apply  change theme
        applyTheme()
    }
    
    // when theme change (dark or normal)
    public func applyTheme() {
        switch (traitCollection.userInterfaceStyle) {
            case .dark:
                contentView.backgroundColor = .clear
                containerView.backgroundColor = .lightGray
                lblTitle.textColor = .white
                backgroundView?.backgroundColor = .black
                break

            case .light:
                contentView.backgroundColor = .clear
                containerView.backgroundColor = .black
                lblTitle.textColor = .white
                backgroundView?.backgroundColor = .clear
                break

            default:
                break
        }
    }
}

