//
//  UpcomingMovieSectionCell.swift
//  designhub-ios
//
//  Created by AGM Tazim on 1/31/22.
//

import UIKit
import RxSwift


typealias UpcomingMovieSectionCellConfig = ListViewCellConfigurator<UpcomingMovieSectionCell, AbstractCellViewModel>

class UpcomingMovieSectionCell : UITableViewCell, ConfigurableCell {
    typealias DataType = AbstractCellViewModel
    
    public var disposeBag = DisposeBag()
    public var offset: CGFloat {
        set{
            collectionView.contentOffset.x = newValue
        }
        get {
            return collectionView.contentOffset.x
        }
    }
    
    public let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        view.isSkeletonable = true
        view.layer.borderWidth = 0
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.cornerRadius = 0
        view.clipsToBounds = true
        return view
    }()
    
    public lazy var collectionView: UICollectionView = {
        let layout = CollectionViewPageFlowLayout()
        layout.scrollDirection = .horizontal
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.isUserInteractionEnabled = true
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.showsVerticalScrollIndicator = false
        collectionView.contentInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        collectionView.isPagingEnabled = true
        collectionView.clipsToBounds = true
        
        //cell registration
        collectionView.rx.setDelegate(self).disposed(by: self.disposeBag)
        collectionView.register(UpcomingMovieItemCell.self, forCellWithReuseIdentifier: UpcomingMovieItemCellConfig.reuseId)
        
        return collectionView
    }()
    
    public lazy var pageControll: UIPageControl = {
        let pageControl = UIPageControl(frame: .zero)
        pageControl.numberOfPages = 0
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        pageControl.currentPageIndicatorTintColor = UIColor.red
        pageControl.pageIndicatorTintColor = UIColor.lightGray.withAlphaComponent(0.8)
        
        return pageControl
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
        collectionView.dataSource = nil
        collectionView.delegate = nil
        collectionView.rx.setDelegate(self).disposed(by: self.disposeBag)
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupSubViews() {
        collectionView.backgroundColor = .clear
        self.backgroundColor = .clear
        
        contentView.addSubview(containerView)
        containerView.addSubview(collectionView)
        containerView.addSubview(pageControll)
        
        contentView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 0, paddingBottom: 10, paddingRight: 0, width: frame.width, height: 300, enableInsets: false)
        containerView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: frame.width, height: contentView.frame.height, enableInsets: false)
        collectionView.anchor(top: containerView.topAnchor, left:  containerView.leftAnchor, bottom: pageControll.topAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: frame.width, height: 275, enableInsets: false)
        pageControll.anchor(top: collectionView.bottomAnchor, left:  containerView.leftAnchor, bottom: containerView.bottomAnchor, right: collectionView.rightAnchor, paddingTop: 10, paddingLeft: 15, paddingBottom: 10, paddingRight: 0, width: 150, height: 0, enableInsets: false)
    }

    public func configure(data: DataType) {
        //config cells
        Observable.of(((data as? UpcomingMovieSectionCellViewModel)?.data) ?? [MovieItemCellViewModel]())
            .observe(on: MainScheduler.instance)
            .bind(to: collectionView.rx.items) { [weak self] collectionView, row, model in
                guard let weakSelf = self else {
                    return UICollectionViewCell()
                }
                
                return weakSelf.populateCollectionViewCell(viewModel: model, indexPath: IndexPath(row: row, section: 0), collectionView: collectionView)
            }.disposed(by: disposeBag)
        
        //configure page controller
        pageControll.numberOfPages = ((data as? UpcomingMovieSectionCellViewModel)?.data?.count) ?? 0
        
        //apply  change theme
        applyTheme()
        
        addSliderTracker()
    }
    
    //populate collection view cell
    private func populateCollectionViewCell(viewModel: AbstractCellViewModel, indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell {
        let item: CellConfigurator = UpcomingMovieItemCellConfig.init(item: viewModel)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        return cell
    }
    
    // MARK: Actions
    private func onTapCollectionviewCell() {
        Observable
            .zip(collectionView.rx.itemSelected, collectionView.rx.modelSelected(UpcomingMovieSectionCellViewModel.self))
            .bind { [weak self] indexPath, model in
                guard let weakSelf = self else {
                    return
                }
                
                debugPrint("Did tap upcoming movie section item cell - \(model.title)")
            }
            .disposed(by: disposeBag)
    }
    
    private func addSliderTracker() {
        collectionView.rx.currentPage
          .bind(to: pageControll.rx.currentPage)
          .disposed(by: disposeBag)
        
        pageControll.rx.controlEvent(.valueChanged)
            .subscribe(onNext: { [weak self] in
                guard let currentPage = self?.pageControll.currentPage else {
                    return
                }
                
                self?.collectionView.scrollToItem(at: IndexPath(row: currentPage, section: 0), at: .centeredHorizontally, animated: true)
            })
            .disposed(by: disposeBag)
    }
    
    // when theme change (dark or normal)
    public func applyTheme() {
        switch (traitCollection.userInterfaceStyle) {
            case .dark:
                contentView.backgroundColor = .midnightBlue
                containerView.backgroundColor = .lightGray
                backgroundView?.backgroundColor = .black
                break

            case .light:
                contentView.backgroundColor = .clear
                containerView.backgroundColor = .clear
                backgroundView?.backgroundColor = .clear
                collectionView.backgroundColor = .clear
                break

            default:
                break
        }
    }
}


extension UpcomingMovieSectionCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: collectionView.frame.width, height: 260)
    }
}

