//
//  GenreSectionCell.swift
//  designhub-ios
//
//  Created by AGM Tazim on 1/31/22.
//

import Foundation
import RxSwift
import RxCocoa
import RxRelay


typealias GenreSectionCellConfig = ListViewCellConfigurator<GenreSectionCell, AbstractCellViewModel>

class GenreSectionCell : UpcomingMovieSectionCell {
    typealias DataType = AbstractCellViewModel
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubViews()
        updateViewConfigs()
        onTapCollectionviewCell()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public func updateViewConfigs() {
        collectionView.isPagingEnabled = false
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        collectionView.collectionViewLayout = layout
        collectionView.register(GenreItemCell.self, forCellWithReuseIdentifier: GenreItemCellConfig.reuseId)
        
        contentView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: frame.width, height: 170, enableInsets: false)
        containerView.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
        collectionView.anchor(top: containerView.topAnchor, left:  containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
        containerView.layer.cornerRadius = 0
        self.backgroundColor = .clear
        
        pageControll.numberOfPages = 0
        pageControll.isHidden = true
    }
    
    public override func configure(data: DataType) {
        //config cells
        Observable.of(((data as? GenreSectionCellViewModel)?.data) ?? [GenreItemCellViewModel]()).observe(on: MainScheduler.instance)
            .bind(to: collectionView.rx.items) { [weak self] collectionView, row, model in
                guard let weakSelf = self else {
                    return UICollectionViewCell()
                }
                
                return weakSelf.populateCollectionViewCell(viewModel: model, indexPath: IndexPath(row: row, section: 0), collectionView: collectionView)
            }.disposed(by: disposeBag)
        
        //apply  change theme
        applyTheme()
    }
    
    //populate collection view cell
    private func populateCollectionViewCell(viewModel: AbstractCellViewModel, indexPath: IndexPath, collectionView: UICollectionView) -> UICollectionViewCell {
        let item: CellConfigurator = GenreItemCellConfig.init(item: viewModel)
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        return cell
    }
    
    // MARK: Actions
    private func onTapCollectionviewCell() {
        Observable
            .zip(collectionView.rx.itemSelected, collectionView.rx.modelSelected(GenreItemCellViewModel.self))
            .bind { [weak self] indexPath, model in
                guard let weakSelf = self else {
                    return
                }
                
                debugPrint("Did tap genre section item vcell - \(model.title)")
            }
            .disposed(by: disposeBag)
    }
    
    // when theme change (dark or normal)
    public override func applyTheme() {
        switch (traitCollection.userInterfaceStyle) {
            case .dark:
                containerView.backgroundColor = .lightGray
                backgroundView?.backgroundColor = .black
                break

            case .light:
                contentView.backgroundColor = .clear
                containerView.backgroundColor = .clear
                backgroundView?.backgroundColor = .clear
                collectionView.backgroundColor = .clear
                break

            default:
                break
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return  CGSize(width: 130, height: 150)
    }
}
