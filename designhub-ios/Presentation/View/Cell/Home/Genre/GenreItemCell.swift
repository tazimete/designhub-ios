//
//  GenreItemCell.swift
//  designhub-ios
//
//  Created by AGM Tazim on 1/31/22.
//

import Foundation
import UIKit


typealias GenreItemCellConfig = ListViewCellConfigurator<GenreItemCell, AbstractCellViewModel>

class GenreItemCell: UICollectionViewCell, ConfigurableCell {
    typealias DataType = AbstractCellViewModel
    
    let containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .midnightBlue
        view.layer.borderWidth = 0
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.cornerRadius = 20
        return view
    }()
    
    let ivThumbnail: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.backgroundColor = .black
        label.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.clipsToBounds = true
        label.layer.cornerRadius = 25
        label.layer.masksToBounds = true 
        return label
    }()
    
    let lblTitle: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 16)
        label.textAlignment = .center
        label.numberOfLines = 1
        return label
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupViews() {
        contentView.addSubview(containerView)
        containerView.addSubview(lblTitle)
        containerView.addSubview(ivThumbnail)
        
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        
        contentView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: frame.width, height: frame.height, enableInsets: false)
        containerView.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: 0, height: 0, enableInsets: false)
        ivThumbnail.anchor(top: containerView.topAnchor, left:  containerView.leftAnchor, bottom: lblTitle.topAnchor, right: containerView.rightAnchor, paddingTop: 20, paddingLeft: 15, paddingBottom: 15, paddingRight: 15, width: 50, height: 50, enableInsets: false)
        lblTitle.anchor(top: ivThumbnail.bottomAnchor, left:  containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 15, paddingRight: 10, width: 0, height: 15, enableInsets: false)
        
//        ivThumbnail.layer.cornerRadius = ivThumbnail.bounds.height/2
    }
    
    func configure(data: AbstractCellViewModel) {
        lblTitle.text = data.title
        ivThumbnail.text = data.thumbnail
    }
}
