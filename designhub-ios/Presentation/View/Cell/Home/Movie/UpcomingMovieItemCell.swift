//
//  UpcomingMovieItemCell.swift
//  designhub-ios
//
//  Created by AGM Tazim on 1/31/22.
//

import Foundation
import UIKit


typealias UpcomingMovieItemCellConfig = ListViewCellConfigurator<UpcomingMovieItemCell, AbstractCellViewModel>

class UpcomingMovieItemCell: UICollectionViewCell, ConfigurableCell {
    typealias DataType = AbstractCellViewModel
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .midnightBlue
        view.isSkeletonable = true
        view.layer.borderWidth = 0
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.cornerRadius = 20
        return view
    }()
    
    let ivPoster : UIImageView = {
        let imgView = UIImageView(image: UIImage(named: "img_avatar"))
        imgView.contentMode = .scaleToFill
        imgView.clipsToBounds = true
        imgView.layer.cornerRadius = 15
        imgView.isSkeletonable = true
        return imgView
    }()
    
    let ivOverlay : UIImageView = {
        let imgView = UIImageView(image: UIImage(named: "img_overlay"))
        imgView.contentMode = .scaleToFill
        imgView.clipsToBounds = true
        imgView.layer.cornerRadius = 15
        imgView.isSkeletonable = true
        return imgView
    }()
    
    let lblTitle : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 17, weight: .bold)
        label.textAlignment = .left
        label.numberOfLines = 2
        label.isSkeletonable = true
        label.skeletonLineSpacing = 10
        label.multilineSpacing = 10
        return label
    }()
    
    let lblReleaseDate : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.isSkeletonable = true
        label.skeletonLineSpacing = 10
        label.multilineSpacing = 10
        return label
    }()
    
    let uivSeparator : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let lblGenre: UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.systemFont(ofSize: 14)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.isSkeletonable = true
        label.skeletonLineSpacing = 10
        label.multilineSpacing = 10
        return label
    }()
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupViews() {
        contentView.addSubview(containerView)
        containerView.addSubview(ivPoster)
        containerView.addSubview(ivOverlay)
        containerView.addSubview(lblTitle)
        containerView.addSubview(lblReleaseDate)
        containerView.addSubview(uivSeparator)
        containerView.addSubview(lblGenre)
        
        contentView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: frame.width, height: frame.height, enableInsets: false)
        containerView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: contentView.frame.width, height: contentView.frame.height, enableInsets: false)
        
        ivPoster.anchor(top: containerView.topAnchor, left:  containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: containerView.frame.width, height: containerView.frame.height, enableInsets: false)
        
        ivOverlay.anchor(top: containerView.topAnchor, left:  containerView.leftAnchor, bottom: containerView.bottomAnchor, right: containerView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0, enableInsets: false)
        
        lblReleaseDate.anchor(top: nil, left:  containerView.leftAnchor, bottom: containerView.bottomAnchor, right: uivSeparator.leftAnchor, paddingTop: 10, paddingLeft: 20, paddingBottom: 10, paddingRight: 5, width: 85, height: 0, enableInsets: false)
        
        uivSeparator.anchor(top: lblReleaseDate.topAnchor, left:  lblReleaseDate.rightAnchor, bottom: lblReleaseDate.bottomAnchor, right: lblGenre.leftAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: 1, height: 15, enableInsets: false)
        
        lblGenre.anchor(top: lblReleaseDate.topAnchor, left:  uivSeparator.rightAnchor, bottom: lblReleaseDate.bottomAnchor, right: containerView.rightAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 10, paddingRight: 10, width: 0, height: 0, enableInsets: false)
        
        lblTitle.anchor(top: nil, left:  containerView.leftAnchor, bottom: lblReleaseDate.topAnchor, right: containerView.rightAnchor, paddingTop: 10, paddingLeft: 20, paddingBottom: 0, paddingRight: 20, width: 0, height: 0, enableInsets: false)
    }
    
    func configure(data: AbstractCellViewModel) {
        let cellViewModel = data as? MovieItemCellViewModel
        ShimmerHelper.startShimmerAnimation(view: ivPoster)
        lblTitle.text = cellViewModel?.title
        lblReleaseDate.text = cellViewModel?.releaseDate
        lblGenre.text = "\(cellViewModel?.voteAverage ?? 0)"
        
        let posterUrl = "\(AppConfig.shared.getServerConfig().getMediaBaseUrl())/\(cellViewModel?.thumbnail ?? "" )"
        ivPoster.loadImage(from: posterUrl, completionHandler: { [weak self] url,image,isFinished  in
            guard let weakSelf = self else {
                return
            }
            
            weakSelf.ivPoster.image = image
            ShimmerHelper.stopShimmerAnimation(view: weakSelf.ivPoster)
        })
    }
}
