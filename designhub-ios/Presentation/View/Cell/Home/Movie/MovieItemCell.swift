//
//  SearchItemCell.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import UIKit
import RxSwift


typealias MovieItemCellConfig = ListViewCellConfigurator<MovieItemCell, AbstractCellViewModel>

class MovieItemCell : UITableViewCell, ConfigurableCell {
    typealias DataType = AbstractCellViewModel
    
    private var disposeBag = DisposeBag()
    var imageUrlAtCurrentIndex: String?
    
    let containerView : UIView = {
        let view = UIView()
        view.backgroundColor = .midnightBlue
        view.isSkeletonable = true
        view.layer.borderWidth = 0
        view.layer.borderColor = UIColor.clear.cgColor
        view.layer.cornerRadius = 15
        view.isUserInteractionEnabled = true
        return view
    }()
    
    let lblTitle : UILabel = {
        let label = UILabel()
        label.textColor = .white
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textAlignment = .left
        label.numberOfLines = 1
        label.isSkeletonable = true
        label.skeletonLineSpacing = 10
        label.multilineSpacing = 10
        return label
    }()
    
    let lblOverview : UILabel = {
        let label = UILabel()
        label.textColor = .lightGray
        label.font = UIFont.systemFont(ofSize: 15)
        label.textAlignment = .left
        label.numberOfLines = 2
        label.isSkeletonable = true
        label.skeletonLineSpacing = 10
        label.multilineSpacing = 10
        return label
    }()
    
    let ivPoster : UIImageView = {
        let imgView = UIImageView(image: UIImage(named: "img_avatar"))
        imgView.contentMode = .scaleAspectFit
        imgView.clipsToBounds = true
        imgView.layer.cornerRadius = 10
        imgView.isSkeletonable = true
        return imgView
    }()
    
    let btnPlay : UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "ic_play"), for: .normal)
        button.backgroundColor = .black
        button.tintColor = .white
        button.contentMode = .scaleAspectFit
        button.clipsToBounds = true
        button.layer.cornerRadius = 20
        button.isSkeletonable = true
        button.isUserInteractionEnabled = true
        return button
    }()
    
    let circularProgressBarView: CircularProgressBarView = {
        let circularProgressBarView = CircularProgressBarView()
        circularProgressBarView.isUserInteractionEnabled = false
        circularProgressBarView.center = CGPoint(x: 10, y: 10)
        circularProgressBarView.createCircularPath()
        return circularProgressBarView
    }()
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        disposeBag = DisposeBag()
        ivPoster.image = nil
        lblTitle.text = ""
        lblOverview.text = ""
        addActionEvent()
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubViews()
        addActionEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setupSubViews() {
        contentView.addSubview(containerView)
        containerView.addSubview(ivPoster)
        containerView.addSubview(lblTitle)
        containerView.addSubview(lblOverview)
        containerView.addSubview(circularProgressBarView)
        containerView.addSubview(btnPlay)
        
        self.backgroundColor = .black
        
        let frameWidth = frame.width
        
        contentView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 5, paddingLeft: 0, paddingBottom: 5, paddingRight: 0, width: frameWidth, height: 115, enableInsets: false)
        containerView.anchor(top: contentView.topAnchor, left: contentView.leftAnchor, bottom: contentView.bottomAnchor, right: contentView.rightAnchor, paddingTop: 5, paddingLeft: 20, paddingBottom: 5, paddingRight: 20, width: frameWidth, height: contentView.frame.height, enableInsets: false)
        ivPoster.anchor(top: containerView.topAnchor, left:  containerView.leftAnchor, bottom: containerView.bottomAnchor, right: nil, paddingTop: 10, paddingLeft: 15, paddingBottom: 10, paddingRight: 0, width: 74, height: 74, enableInsets: false)
        lblTitle.anchor(top: ivPoster.topAnchor, left: ivPoster.rightAnchor, bottom: lblOverview.topAnchor, right: btnPlay.leftAnchor, paddingTop: 0, paddingLeft: 10, paddingBottom: 0, paddingRight: 10, width: frameWidth-30, height: 0, enableInsets: false)
        lblOverview.anchor(top: lblTitle.bottomAnchor, left: ivPoster.rightAnchor, bottom: ivPoster.bottomAnchor, right: btnPlay.leftAnchor, paddingTop: 10, paddingLeft: 10, paddingBottom: 15, paddingRight: 10, width: frameWidth-30, height: 0, enableInsets: false)
        btnPlay.anchor(top: ivPoster.topAnchor, left: lblTitle.rightAnchor, bottom: nil, right: containerView.rightAnchor, paddingTop: 15, paddingLeft: 5, paddingBottom: 15, paddingRight: 15, width: 40, height: 40, enableInsets: false)
        circularProgressBarView.anchor(top: btnPlay.topAnchor, left: btnPlay.leftAnchor, bottom: btnPlay.bottomAnchor, right: btnPlay.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 40, height: 40, enableInsets: false)
    }

    public func configure(data: DataType) {
        ShimmerHelper.startShimmerAnimation(view: ivPoster)
        lblTitle.text = data.title
        lblOverview.text = data.overview
        
        let posterUrl = "\(AppConfig.shared.getServerConfig().getMediaBaseUrl())/\(data.thumbnail ?? "" )"
        imageUrlAtCurrentIndex = posterUrl
        ivPoster.loadImage(from: posterUrl, completionHandler: { [weak self] url,image,isFinished  in
            guard let weakSelf = self else {
                return
            }
            
            weakSelf.ivPoster.image = image
            ShimmerHelper.stopShimmerAnimation(view: weakSelf.ivPoster)
        })
        
        //apply  change theme
        applyTheme()
    }
    
    private func addActionEvent() {
        btnPlay.rx.tap.subscribe(onNext: { [weak self] in
            self?.circularProgressBarView.progressAnimation(duration: 3)
        }).disposed(by: disposeBag)
    }
    
    // when theme change (dark or normal)
    public func applyTheme() {
        switch (traitCollection.userInterfaceStyle) {
            case .dark:
                contentView.backgroundColor = .clear
                containerView.backgroundColor = .lightGray
                lblTitle.textColor = .white
                lblOverview.textColor = .white
                backgroundView?.backgroundColor = .black
                break

            case .light:
                contentView.backgroundColor = .clear
                containerView.backgroundColor = .midnightBlue
                lblTitle.textColor = .white 
                lblOverview.textColor = .lightGray
                backgroundView?.backgroundColor = .black
                break

            default:
                break
        }
    }
}


