//
//  SearchShimmerCell.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import UIKit


typealias ShimmerItemCellConfig = ListViewCellConfigurator<MovieShimmerCell, AbstractCellViewModel>

class MovieShimmerCell: MovieItemCell {
    typealias DataType = AbstractCellViewModel
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func configure(data: DataType) {
        applyTheme()
        btnPlay.isHidden = true
        circularProgressBarView.isHidden = true 
        containerView.layer.borderWidth = 0
        containerView.layer.borderColor = UIColor.clear.cgColor
        
        //shmmer skeleton animation
        ShimmerHelper.startShimmerAnimation(viewlist: [lblTitle, lblOverview, ivPoster])
    }
    
    // when theme change (dark or normal)
    override public func applyTheme() {
        switch (traitCollection.userInterfaceStyle) {
            case .dark:
                containerView.backgroundColor = .black
                containerView.layer.borderColor = UIColor.white.cgColor
                backgroundView?.backgroundColor = .black
                break

            case .light:
                containerView.backgroundColor = .clear
                containerView.layer.borderColor = UIColor.clear.cgColor
                break

            default:
                break
        }
    }
}
