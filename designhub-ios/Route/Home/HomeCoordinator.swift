//
//  RootCoordinator.swift
//  designhub-ios
//
//  Created by AGM Tazim on 28/01/22.
//

import UIKit

class HomeCoordinator: Coordinator {
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }

    public func start() {
        let movieRepository = MovieRepository(apiClient: APIClient.shared)
        let genreRepository = GenreRepository(apiClient: APIClient.shared)
        let movieUsecase = MovieUsecase(repository: movieRepository)
        let genreUsecase = GenreUsecase(repository: genreRepository)
        let viewModel = HomeViewModel(usecase: movieUsecase, genreUsecase: genreUsecase)
        let vc = HomeViewController(viewModel: viewModel)
        self.navigationController.pushViewController(vc, animated: true)
    }
}
